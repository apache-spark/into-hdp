package into

import org.apache.spark.sql.SparkSession

object SparkDemo {
  def main(args: Array[String]): Unit = {

    val spark = SparkSession.builder()
      .appName("IntoSparkApp")
      .config("spark.hadoop.hive.zookeeper.quorum", "hdp3.c.test:2181")
      .getOrCreate()

    try {
      val df = spark.read.format("csv").option("header", "true")
        .load("/tmp/test.csv")
      df.show(false)

    } catch {
      case _: Throwable => println("Some error happened.")
    }

    spark.stop()
  }
}